syntax = "proto3";

package user.user;

import "shared_message.proto";

message Account {
  enum IdentifierType {
    DEFAULT_IDENTITY = 0;
    IDENTITY = 1;
    NAVER = 2;
    KAKAO = 3;
    GOOGLE = 4;
  }
  IdentifierType identifier_type = 1;
  string identifier = 2;
  optional string password = 3;
}

message CreateUserRequest {
  repeated TermResult terms = 1;
  Account account = 2;
  PersonalInformation personal_information = 3;
}

message ListProfileRequest {
  int32 offset = 1;
  int32 limit = 2;
  optional string order= 3;
  optional string user_name = 4;
  optional string profile_nick_name = 5;
  optional int32 roleLevel = 6;
  optional string location = 7;
}

message ListUserRelationship {
  int32 count = 1;
  repeated UserRelationship user_relationships = 2;
}

message ListUserRequest {
  int32 offset = 1;
  int32 limit = 2;
  optional string order = 3;
  optional string name = 4;
  optional int32 role_level = 5;
}

message ListSearchableUserRequest {
  message SearchQuery {
    optional string name = 1;
    optional string mobile = 2;
    optional int32 role_level = 3;
    repeated string order = 4;
  }
  SearchQuery search_query = 1;
  optional sharedmessage.PagingRequest paging_request = 2;
}

message ListSearchableUserResponse {
  message RestrictedUserResponse {
    int32 id = 1;
    repeated int32 role_levels = 2;
    optional string name = 3;
    optional string mobile = 4;
  }  
  repeated RestrictedUserResponse users = 1;
  sharedmessage.PagingRequest paging_request = 2;
}

message PersonalInformation {
  string name = 1;
  string mobile = 2;
}

message Profile {
  int32 id = 1;
  int32 user_id = 2;
  string nickname = 3;
  string image_url = 4;
  string background_image_url = 5;
  string comment = 6;
  string description = 7;
  bool is_active = 8;
}

message ProfileList {
  int32 count = 1;
  repeated Profile profiles = 2;
}

message UpdateUserProfileRequest {
  int32 user_id = 1;
  optional string nickname = 2;
  optional string image_url = 3;
  optional string background_image_url = 4;
  optional string comment = 5;
  optional string description = 6;
  optional bool is_active = 7;
}

message TermResult {
  int32 term_id = 1;
  string version = 2;
  bool is_required = 3;
  bool is_agree = 4;
}

message UpdateUserRelationshipRequest {
  int32 id = 1;
  optional UserRelationshipStatus status = 2;
  optional int32 user_grade_id = 3;
  optional string comment = 4;
}

message UpdatePasswordRequest {
  Account account = 1;
  PersonalInformation personal_information = 2;
}

message UserAndProfileId {
  int32 user_id = 1;
  int32 profile_id = 2;
}

// ☠️ REST API 파라미터 값과 연계되어 있어, user_id, partner_id 순서 변경 및 별도의 message type으로 변경할 수 없음
message UserId {
  int32 user_id = 1;
}

message UserIdentity {
  string identity = 1;
}

enum UserRelationshipType {
  DEFAULT_DIRECT = 0;
  DIRECT = 1; // direct contract trainer(parentId) and user(childId)
  VIA_AGENT = 2; // trainer(parentId) connected to user(childId) via agent
  EMPLOYMENT = 3; // agent or center(parentId) employ trainer(childId)
  REGISTRATION = 4; // agent or center(parentId) registrater user(childId)
}
enum UserRelationshipStatus {
  DEFAULT_REQUESTED = 0;
  REQUESTED = 1;
  CONNECTED = 2;
  DISCONNECTED = 3;
}

message UserRelationship { 
  int32 parent_id = 1; // trainer or agent must be a parent
  int32 child_id = 2;
  UserRelationshipType type = 3;
  UserRelationshipStatus status = 4;
  int32 parent_role_level = 5;
  int32 child_role_level = 6;
  int32 user_grade_id = 7;
  optional string comment = 8;
  optional bool is_active = 9;
  optional string created_at = 10; 
  optional string updated_at = 11;
}

message UserRelationshipPairIds {
  int32 user_id = 1;
  int32 partner_id = 2;
}

message CreateUserRelationship {
  int32 parent_id = 1;
  int32 child_id = 2;
  UserRelationshipType type = 3;
  UserRelationshipStatus status = 4;
  int32 parent_role_level = 5;
  int32 child_role_level = 6;
  int32 user_grade_id = 7;
  string comment = 8;
}

message UserRelationshipRequest {
  int32 user_id = 1;
  int32 partner_id = 2;
}

message UserRelationshipResponse {
  int32 id = 1;
  UserRelationshipType type = 2;
  UserRelationshipStatus status = 3;
  int32 user_grade_id = 4;
  int32 parent_id = 5;
  int32 parent_role_level = 6;
  int32 child_id = 7;
  int32 child_role_level = 8;
  string comment = 9;
}

message GetUserRequest {
  int32 user_id = 1;
}

message GetUserResponse {
  int32 id = 1;
  repeated int32 role_levels = 2;
  string name = 3;
}

message GetProfileRequest {
  int32 user_id = 1;
}

message GetProfileResponse {
  int32 id = 1;
  int32 user_id = 2;
  string nickname = 3;
  string image_url = 4;
  string background_image_url = 5;
  string comment = 6;
  string description = 7;
  bool is_active = 8; 
}

message ListUserRelationshipRequest {
  int32 user_id = 1;
  optional string query = 2;
}

message ListUsersRequest {
  optional string name = 1;
  optional int32 partner_id = 2;
}

message ListUsersResponse {
  message RestrictedUserResponse {
    int32 id = 1;
    repeated int32 role_levels = 2;
    string name = 3;
  }  
  repeated RestrictedUserResponse users = 1;
}

message ListPartnersRequest {
  optional string name = 1;
  optional string mobile = 2;
  repeated int32 role_levels = 3;
}

message ListPartnersResponse {
  message User {
    int32 id = 1;
    repeated int32 role_levels = 2;
    string name = 3;
  }
  repeated User users = 1;
}

message ListUserRelationshipByPartnerRequest {
  int32 partner_id = 1;
}

message GetUserRelationshipByIdRequest {
  int32 id = 1;
}

message GetUserRelationshipByKeyRequest {
  int32 parent_id = 1;
  int32 type = 2;
  int32 child_id = 3;
}

message DeleteUserRelationshipRequest {
  int32 id = 1;
}

message CreateUserRoleRequest {
  int32 role_level = 1;
}

message ListUserRelationshipByPartnerResponse {
  repeated UserRelationshipResponse data = 1;
}